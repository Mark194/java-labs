<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: zheny
  Date: 14.12.2018
  Time: 9:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Welcome!</title>
</head>
<body>

<h1>${message}</h1>
<form method="post" action="${pageContext.request.contextPath}/users">
  <input type="submit" value="exit">
</form>
<label>Recently logged in:</label>
<ol>
  <c:forEach items="${users}" var="user">
    <li>${user}</li>
  </c:forEach>
</ol>
</body>
</html>
