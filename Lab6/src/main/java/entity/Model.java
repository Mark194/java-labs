package entity;

import java.util.ArrayList;
import java.util.List;

public class Model {

    private List<User> users;

    public Model(){
        users = new ArrayList <User>();
    }

    public void addUsers(User user){
        users.add(user);
    }

    public ArrayList<String> getUsersLogin(){
        ArrayList<String> logins = new ArrayList <String>();
        for (User user : users){
            logins.add(user.getLogin());
        }
        return logins;
    }

    public List<User> getUsers(){
        return users;
    }
}
