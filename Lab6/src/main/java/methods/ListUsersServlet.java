package methods;

import entity.Model;
import entity.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ListUsersServlet extends HttpServlet {
    Model model;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        model = (Model) getServletConfig().getServletContext().getAttribute("model");
        getServletContext().setAttribute("users", model.getUsersLogin());
        this.getServletContext().getRequestDispatcher("/Users.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().getServletContext().setAttribute("loginUser",null);
        req.getSession().getServletContext().setAttribute("password",null);
        resp.sendRedirect("/");
    }


}
