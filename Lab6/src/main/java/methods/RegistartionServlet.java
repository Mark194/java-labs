package methods;

import entity.Model;
import entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegistartionServlet extends HttpServlet {
    int status = 0;
    Model model = new Model();
    User user;
    String message;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("Registration.jsp");
        if (req.getAttribute("model") == null) {
            getServletContext().setAttribute("model", model);
        } else {
            model = (Model)getServletConfig().getServletContext().getAttribute("model");
        }
        if (req.getSession().getServletContext().getAttribute("loginUser") == null || req.getSession().getServletContext().getAttribute("password") == null)
            requestDispatcher.forward(req, resp);
        else
        if (!findUser()) {
            model.getUsers().add(user);
            status = 1;
            setMessage();
            resp.sendRedirect("Users.jsp");
        }
        else {
            setMessage();
            if (authorization()) {
                status = 0;
                setMessage();
                resp.sendRedirect("Users.jsp");
            }
            else {

                resp.sendRedirect("Registration.jsp");
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        req.getSession().getServletContext().setAttribute("loginUser",req.getParameter("login"));
        req.getSession().getServletContext().setAttribute("password",req.getParameter("password"));
        user = new User(req.getParameter("login"),req.getParameter("password"));
        resp.sendRedirect("/");

    }

    private boolean findUser(){
        boolean status = false;
        for (User user1 : model.getUsers())
            if (user1.getLogin().equals(user.getLogin())) {
                status = true;
                break;
            }
            return status;
    }

    private boolean authorization(){
        boolean status = false;
        for (User user1 : model.getUsers()){
            if (user1.getLogin().equals(user.getLogin()))
                if (user1.getPassword().equals(user.getPassword())) {
                    status = true;
                    break;
                }
        }
        return status;
    }

    private void setMessage(){
        if (status == 1)
            message = "Congratulations on your successful registration, " + user.getLogin() + "!";
         else
            message = "Hello, "+user.getLogin()+"!";
        getServletContext().setAttribute("message", message);
        getServletContext().setAttribute("users", model.getUsersLogin());
    }

}
