package rsreu.markov.java.system.control.methods;

import rsreu.markov.java.system.control.entity.Port;
import rsreu.markov.java.system.control.entity.Ship;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class CommandPort {private Semaphore semaphore;
    private Port port;
    private ReentrantLock reentrantLock = new ReentrantLock();
    private int countOfLeft = 0;
    private int depotGeneral = 0;
    private int countSet = 0;

    public CommandPort(Semaphore semaphore, Port port){
        this.port = port;
        this.semaphore = semaphore;
    }

    public void takeShip (Ship ship) {
        try {
            semaphore.acquire();
        }
        catch (InterruptedException ie){
            System.out.println("Failed acquire!!!");
            Thread.currentThread().interrupt();
        }
        reentrantLock.lock();
        takeDock(ship);
        reentrantLock.unlock();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException ie){
            System.out.println("Take ship "+Thread.currentThread().getName()+" failed!");
            Thread.currentThread().interrupt();
        }
    }

    public void freeShip(Ship ship){
        reentrantLock.lock();
        if (ship.getAmountOfCargo() > (port.getDepotCapacity()/2)){
            outMessage(" left ",ship);
            port.getDocks().get(ship.getDockNumber()).setIsFree(false);
            reentrantLock.unlock();
            semaphore.release();
        } else {
            if (ship.getIsFull()){
                dischargeCargo(ship);
            } else {
                freeCargo(ship);
            }
        }
    }

    private void takeDock(Ship ship){
        boolean status = false;
            for (int i = 0;(!status && i < port.getNumberOfDocks()); i++) {
                if (!port.getDocks().get(i).getIsFree().get()) {
                    port.getDocks().get(i).setIsFree(true);
                    ship.setDockNumber(i);
                    status = true;
                }
            }
        outMessage(" has entered ship ", ship);
    }

    private void dischargeCargo(Ship ship){
        if (port.getFreeDepotCapacity() >= ship.getAmountOfCargo()) {
            port.setDepot(port.getDepot() + ship.getAmountOfCargo());
            depotGeneral += ship.getAmountOfCargo();
            System.out.println("Set depot: "+ship.getAmountOfCargo());
            ship.setAmountOfCargo(0);
            outMessage(" left ", ship);
            countOfLeft++;
            port.getDocks().get(ship.getDockNumber()).setIsFree(false);
            reentrantLock.unlock();
            semaphore.release();
        } else {
            expect(ship);
        }
    }

    private void freeCargo(Ship ship) {
        if (port.getDepot() - ship.getAmountOfCargo() > 0 ){
                port.setDepot(port.getDepot() - ship.getAmountOfCargo());
                port.getDocks().get(ship.getDockNumber()).setIsFree(false);
                depotGeneral += ship.getAmountOfCargo();
                countOfLeft++;
                outMessage(" left ", ship);
                System.out.println("Put depot: "+ship.getAmountOfCargo());
                reentrantLock.unlock();
                semaphore.release();

        } else {
            port.setDepot(port.getDepotCapacity()/2);
            depotGeneral+=port.getDepotCapacity()/2;
            countSet++;
            expect(ship);
        }
    }



    private void expect(Ship ship){
        System.out.println(Thread.currentThread().getName()+" wait...");
        try {
            TimeUnit.MILLISECONDS.sleep(1);
        } catch(InterruptedException ie){
            System.out.println(Thread.currentThread().getName()+" wait with error!");
            Thread.currentThread().interrupt();
        }
        reentrantLock.unlock();
        freeShip(ship);
    }

    private void outMessage(String message, Ship ship){
        System.out.println("In dock "+ ship.getDockNumber()+message+Thread.currentThread().getName()+" on "+ship.getIsFull());
    }

    public int getCountOfLeft() {
        return countOfLeft;
    }

    public int getDepotGeneral() {
        return depotGeneral;
    }

    public int getCountSet() {
        return countSet;
    }
}
