package rsreu.markov.java.system.control.methods;

import rsreu.markov.java.system.control.entity.Port;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Loader extends Thread {
    private Port port;
    private ReentrantLock reentrantLock = new ReentrantLock();
    private int countFree = 0;
    private int count = 0;

    public Loader(Port port){
        this.port = port;
    }

    @Override
    public void run() {
        while (true) {
            reentrantLock.lock();
            checkPortCargo();
            reentrantLock.unlock();
            try {
                TimeUnit.MILLISECONDS.sleep(5);
            } catch (InterruptedException ie) {
                System.out.println("Error demon!");
                Thread.currentThread().interrupt();
            }

        }
    }

    public void checkPortCargo(){
            if (port.getDepot() >= port.getDepotCapacity() - 10) {
                port.setDepot(0);
                countFree++;
            }
            if (port.getDepot() <= 0 ) {
                port.setDepot(port.getDepotCapacity()/3);
                count++;
            }
    }

    public int getCountFree() {
        return countFree;
    }

    public int getCount() {
        return count;
    }
}
