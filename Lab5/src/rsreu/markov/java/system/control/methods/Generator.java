package rsreu.markov.java.system.control.methods;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public interface Generator {
    ArrayList<Callable<Integer>> generate(CommandPort commandPort);
}
