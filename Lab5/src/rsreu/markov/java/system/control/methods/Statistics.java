package rsreu.markov.java.system.control.methods;

import rsreu.markov.java.system.control.entity.Port;

public class Statistics {
    private int count;
    private int countOfSuccessful;
    private Port port;

    public Statistics (int count, Port port){
        this.count = count;
        this.port = port;
    }

    public void showInitial(){
        System.out.println("Total depot at the system is "+port.getDepotCapacity());
    }

    public void showCountOfCargo(String message){
        System.out.println(message);
        System.out.println(port.getDepotCapacity()+"\\"+port.getDepot());
    }

    public void showStatistics(){
        String message;
        if (count == countOfSuccessful)
            message = "Congratulations!";
        else
            message = "Attantion!!!";
        System.out.println(message);
        System.out.println(countOfSuccessful+" out of "+count+" ships entered and left the port.");

    }

    public void showAllDepot(int count){
        System.out.println("Total arrived at the port "+count+" depot.");
    }

    public void showCountOfFree(int count, String message){
        if (count != 0)
            System.out.println("Depot "+message+" at the port: "+count);
        else
            System.out.println("Depot "+message+" in the port was not.");
    }

    public void setCountOfSuccessful(int countOfSuccessful) {
        this.countOfSuccessful = countOfSuccessful;
    }
}
