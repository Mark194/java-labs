package rsreu.markov.java.system.control.methods;

import rsreu.markov.java.system.control.entity.Ship;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class GeneratorShip implements Generator {
    ArrayList<Callable<Integer>> collections = new ArrayList <Callable<Integer>>();

    @Override
    public ArrayList <Callable <Integer>> generate(CommandPort commandPort) {
        int count = (int)(Math.random()*10+2);
        for (int i = 0; i < count; i++) {
            collections.add(new Ship((int) (Math.random()*18+1), generateBoolean((int)(Math.random()*18+1)), commandPort));
      }
        return collections;
    }

    public boolean generateBoolean(int number ) {
        if ((number%2)==0)
            return true;
        else
            return false;

    }
}
