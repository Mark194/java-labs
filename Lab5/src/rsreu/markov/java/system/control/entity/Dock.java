package rsreu.markov.java.system.control.entity;

import java.util.concurrent.atomic.AtomicBoolean;

public class Dock {
    private AtomicBoolean isFree;

    public AtomicBoolean getIsFree() {
        return isFree;
    }

    public void setIsFree(boolean isFree) {
        this.isFree = new AtomicBoolean(isFree);
    }

    @Override
    public boolean equals(Object object){
        if (this == object)
            return true;
        if (object == null || this.getClass() != object.getClass())
            return false;
        Dock dock = (Dock) object;
        return this.getIsFree().equals(dock.getIsFree());
    }

    @Override
    public int hashCode(){
        return isFree.hashCode();
    }
}
