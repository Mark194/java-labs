package rsreu.markov.java.system.control.entity;

import java.util.ArrayList;

public class Port {
    private static final int NUMBER_OF_DOCKS = 7;
    private static final int DEPOT_CAPACITY = 60;
    private ArrayList<Dock> docks = new ArrayList<>(NUMBER_OF_DOCKS);
    private int depot = 0;

   public Port() {
        for (int i=0; i < NUMBER_OF_DOCKS; i++){
            docks.add(new Dock());
            docks.get(i).setIsFree(false);
        }
    }

    public int getDepot() {
        return depot;
    }

    public void setDepot(int depot) {
        this.depot = depot;
    }

    public int getNumberOfDocks(){
        return NUMBER_OF_DOCKS;
    }

    public int getDepotCapacity(){
        return DEPOT_CAPACITY;
    }

    public ArrayList<Dock> getDocks(){
        return docks;
    }

    public int getFreeDepotCapacity(){
        return DEPOT_CAPACITY-depot;
    }

}
