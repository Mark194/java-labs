package rsreu.markov.java.system.control.entity;


import rsreu.markov.java.system.control.methods.CommandPort;

import java.util.concurrent.Callable;

public class Ship implements Callable <Integer> {
    private int amountOfCargo, dockNumber;
    private boolean isFull;
    private CommandPort commandPost;

    public Ship (int countOfCargo, boolean isFull, CommandPort commandPost1) {
        this.amountOfCargo = countOfCargo;
        this.isFull = isFull;
        this.commandPost = commandPost1;
    }

    @Override
    public Integer call() throws Exception {
        int num = this.amountOfCargo;
        commandPost.takeShip(this);
        commandPost.freeShip(this);
        return num;
    }

    public int getAmountOfCargo() {
        return amountOfCargo;
    }

    public void setAmountOfCargo(int amountOfCargo) {
        this.amountOfCargo = amountOfCargo;
    }

    public CommandPort getCommandPost() {
        return commandPost;
    }

    public int getDockNumber() {
        return dockNumber;
    }

    public void setDockNumber(int dockNumber) {
        this.dockNumber = dockNumber;
    }

    public boolean getIsFull() {
        return isFull;
    }
}
