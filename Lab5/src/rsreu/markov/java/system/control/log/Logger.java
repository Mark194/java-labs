package rsreu.markov.java.system.control.log;

import com.sun.org.glassfish.external.statistics.Statistic;
import rsreu.markov.java.system.control.entity.Port;
import rsreu.markov.java.system.control.entity.Ship;
import rsreu.markov.java.system.control.methods.*;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;


public class Logger {

    private Port port = new Port();
    private final Semaphore semaphore = new Semaphore(port.getNumberOfDocks(), true);
    private ExecutorService executorService = Executors.newCachedThreadPool();
    private CommandPort commandPort = new CommandPort(semaphore, port);

    public static void main(String[] args) {
        Logger logger = new Logger();
        logger.run();
    }

    public void run(){
        Loader storageManager = new Loader(port);
        Generator generator = new GeneratorShip();
        ArrayList<Callable<Integer>> collection = generator.generate(commandPort);
        Statistics statistics = new Statistics(collection.size(), port);
        try {
            storageManager.setDaemon(true);
            statistics.showInitial();
            statistics.showCountOfCargo("Load statistics at start of execution:");
            storageManager.start();
            executorService.invokeAll(collection);
            executorService.shutdown();
        } catch (InterruptedException e) {
            System.out.println("error");
        }
        statistics.showCountOfCargo("Load statistics at finish of execution:");
        statistics.setCountOfSuccessful(commandPort.getCountOfLeft());
        statistics.showCountOfFree(storageManager.getCountFree(), "release");
        statistics.showCountOfFree(storageManager.getCount(), "set");
        statistics.showAllDepot(commandPort.getDepotGeneral()+commandPort.getCountSet());
        statistics.showStatistics();
    }
}
