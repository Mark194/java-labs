package rsreu.markov.java.app.loading;

import rsreu.markov.java.app.data.BookData;
import rsreu.markov.java.app.data.ModifiedBookData;
import rsreu.markov.java.app.data.PropertiesData;
import rsreu.markov.java.app.data.ReadBookData;
import rsreu.markov.java.app.data.ReadPropertiesData;
import rsreu.markov.java.app.data.WriteBookData;
import rsreu.markov.java.app.data.backup.BookBackup;
import rsreu.markov.java.app.data.backup.CreateBookBackup;
import rsreu.markov.java.app.parse.Parse;
import rsreu.markov.java.app.parse.ParseBook;

public class LoadingApp {
	
	public static void main(String[] arg) {
		LoadingApp load = new LoadingApp();
		load.init();
	}
	
	void init() {
		BookBackup backup = new CreateBookBackup();
		backup.readBookFile();
		backup.createBackup();
		BookData read = new ReadBookData();
		read.setFolder(backup.getFolder());
		read.read();
		Parse parse = new ParseBook();
		parse.setSymbols(read.getSymbols());
		parse.parseText();
		parse.outTable();
		ModifiedBookData modified = new WriteBookData();
		modified.setTable(parse.getTable(), parse.getLength());
		modified.write();
		parse.findWordOfMaxSymbol();
		PropertiesData d = new ReadPropertiesData();
	}
}
