package rsreu.markov.java.app.data;

public interface ModifiedBookData {
	void write();
	void setTable(String[] table, int length);
}
