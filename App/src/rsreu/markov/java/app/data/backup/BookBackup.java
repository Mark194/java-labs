package rsreu.markov.java.app.data.backup;

public interface BookBackup {
	void readBookFile();
	void createBackup();
	String getFolder();
}
