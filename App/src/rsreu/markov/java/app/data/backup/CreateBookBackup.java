package rsreu.markov.java.app.data.backup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreateBookBackup implements BookBackup {
	private String folder = "C://���������_��_���//App//resource//original//book.txt";
	private byte[] d = null;
	
	public String getFolder() {
		return this.folder;
	}
	
	
	public void readBookFile() {
		
		try {
			FileInputStream fis = new FileInputStream(folder);
			System.out.println("The file is ready to read!");
			int bytesAvailable = fis.available();
			d = new byte[bytesAvailable];
			System.out.println("Ready to count "+bytesAvailable);
			ByteArrayInputStream bais = new ByteArrayInputStream(d);
			int count = fis.read(d,0,bytesAvailable);
			System.out.println("Counted "+count);
			fis.close();
			bais.close();
		}
		catch(FileNotFoundException e) {
			System.out.println("\r\n" + "Can not find the specified file!");
		}
		catch(IOException e) {
			System.out.println(e);	
		}
		
	}
	
	public void createBackup()  {
		FileOutputStream fos = null;
		ByteArrayOutputStream dd = new ByteArrayOutputStream();
		String folder = "C://���������_��_���//App//resource//backup//book.bak";
		try {
			fos = new FileOutputStream(folder);
			dd.write(d, 0, d.length);
			dd.writeTo(fos);
		}
		catch(FileNotFoundException e) {
			System.out.println("Can not find the specified file!");
			System.exit(0);
		}
		catch(IOException e) {
			System.out.println(e);
			System.exit(0);
		}
		catch (NullPointerException f) {
			System.out.println("File not founded!");
			System.exit(0);
		}
		finally {
			try {
				dd.close();
				fos.close();
				System.out.println("Backup is created!");
				System.out.println("File is closed!");
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
}
