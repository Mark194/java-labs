package rsreu.markov.java.app.data;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class ReadPropertiesData implements PropertiesData {
	ArrayList<String> regular = new ArrayList<>();
	public void readProperty() {
		Locale loc = new Locale("en","US");
		ResourceBundle crunchifyResourceBundle = ResourceBundle.getBundle("regular",loc);
		Enumeration<String> crunchifyKeys = crunchifyResourceBundle.getKeys();
		while (crunchifyKeys.hasMoreElements()) {
			String crunchifyKey = crunchifyKeys.nextElement();
			regular.add(crunchifyResourceBundle.getString(crunchifyKey));
		}
	}
	public ArrayList<String> getRegular() {
		return regular;
	}
}
