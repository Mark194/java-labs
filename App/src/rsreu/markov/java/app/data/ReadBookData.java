package rsreu.markov.java.app.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import rsreu.markov.java.app.element.Symbol;

public class ReadBookData implements BookData {
	private String folder;
	private ArrayList<Symbol> symbols = new ArrayList<>();
	
	public ArrayList<Symbol> getSymbols() {
		return symbols;
	}
	
	public void setSymbols(ArrayList<Symbol> symbols) {
		this.symbols = symbols;
	}
	
	public void read() {
		File f = new File(this.folder);
		FileReader reader = null;
		try { 
			System.out.println("The file is open.");
			reader = new FileReader(f);
			while(reader.ready()) {
				Symbol e = new Symbol();
				e.setSymbol((char)reader.read());
				this.symbols.add(e);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Cann't open file!");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Input/output error!");
			System.exit(0);
		} 
		finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Data from the file received!");
			System.out.println("File is closed!");
		}

	}


	public void setFolder(String folder) {
		this.folder = folder;
	}
	
}
