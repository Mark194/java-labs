package rsreu.markov.java.app.data;

import java.util.ArrayList;

public interface PropertiesData {
	void readProperty();
	ArrayList<String> getRegular();
}
