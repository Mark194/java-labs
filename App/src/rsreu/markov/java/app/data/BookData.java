package rsreu.markov.java.app.data;

import java.util.ArrayList;

import rsreu.markov.java.app.element.Symbol;

public interface BookData {
	 void setFolder(String folder);
	 void read();
	 ArrayList<Symbol> getSymbols();
}
