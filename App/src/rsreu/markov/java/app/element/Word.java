package rsreu.markov.java.app.element;

import java.util.ArrayList;

public class Word extends Element {
	private ArrayList<Symbol> symbols;
	
	public Word(String c) {
		symbols = new ArrayList<>(c.length());
		for (int i=0; i<c.length(); i++)
			symbols.add(new Symbol(c.charAt(i)));
	}
	
	public ArrayList<Symbol> getValue(){
		return this.symbols;
	}
	
	public void setSymbols(ArrayList<Symbol> symbols) {
		this.symbols = symbols;
	}
	
	public String toString() {
		String word = "";
		for (Symbol s : symbols) {
			word+=s.getSymbol();
		}
		return word;
	}
	
}
