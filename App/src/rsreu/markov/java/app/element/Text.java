package rsreu.markov.java.app.element;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rsreu.markov.java.app.data.PropertiesData;
import rsreu.markov.java.app.data.ReadPropertiesData;

public class Text {
	private String text;
	private ArrayList<Sentence> sentences = new ArrayList<>();
	
	public Text(String text) {
		this.text=text;
		PropertiesData prop = new ReadPropertiesData();
		prop.readProperty();
		Pattern p = Pattern.compile(prop.getRegular().get(1));
		Matcher m = p.matcher(text);
		while(m.find()) {
			sentences.add(new Sentence(m.group().toString()));
		}
	}
	
	public ArrayList<Sentence> getSentences(){
		return this.sentences;
	}
	
	public int getCountSentences() {
		return sentences.size();
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
