package rsreu.markov.java.app.element;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rsreu.markov.java.app.data.PropertiesData;
import rsreu.markov.java.app.data.ReadPropertiesData;

public class Sentence extends Element {
	private ArrayList<Element> elements = new ArrayList<>();
	private ArrayList<Word> words = new ArrayList<>();
	
	public Sentence(String sentence) {
		PropertiesData prop = new ReadPropertiesData();
		prop.readProperty();
		Pattern p = Pattern.compile(prop.getRegular().get(0)); 
		Pattern p2 = Pattern.compile(prop.getRegular().get(2));
		Matcher m = p.matcher(sentence);
		Matcher m2 = p2.matcher(sentence);
		int position=0;
		while (m.find())
		{
			if (!m.hitEnd()) {
				position=m.end();
				elements.add(new Word(m.group()));
				words.add(new Word(m.group()));
			}
			while (m2.find()) {
				if ((position)==m2.start()) {
					elements.add(new Symbol(m2.group().charAt(0)));
					break;
				}
			}
			m2.reset();
		}
	}
	public ArrayList<Element> getSentence() {
		return elements;
	}
	
	public ArrayList<Word> getWords() {
		return words;
	}

}
