package rsreu.markov.java.app.element;

public class Symbol extends Element {
	private char symbol;
	
	public Symbol() {
		
	}
	
	public Symbol(char c) {
		this.symbol=c;
	}

	public char getSymbol() {
		return symbol;
	}

	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}
	
	public String toString() {
		String c = new String(this.symbol+"");
		return c;
	}
}
