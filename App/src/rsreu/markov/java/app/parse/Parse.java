package rsreu.markov.java.app.parse;

import java.util.ArrayList;

import rsreu.markov.java.app.element.Symbol;

public interface Parse {
	void setSymbols(ArrayList<Symbol> otherSymbols);
	void outTable();
	void parseText();
	String[] getTable();
	int getLength();
	void findWordOfMaxSymbol();
}
