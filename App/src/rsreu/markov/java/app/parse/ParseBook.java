package rsreu.markov.java.app.parse;

import java.util.ArrayList;
import java.util.Arrays;
import rsreu.markov.java.app.element.Symbol;
import rsreu.markov.java.app.element.Text;
import rsreu.markov.java.app.element.Word;
import rsreu.markov.java.app.element.Element;
import rsreu.markov.java.app.element.Sentence;

public class ParseBook implements Parse {
	private Text text; 
	final int length = 42;
	final int sizeRow1 = 5;
	final int sizeRow2 = 14;
	final int size=length+sizeRow1+sizeRow2;
	private String []table;
	private ArrayList<Symbol> symbols = new ArrayList<>();
	
	public void setSymbols(ArrayList<Symbol> otherSymbols) {
		this.symbols=otherSymbols;
	}	
	
	public String[] getTable() {
		return this.table;
	}
		public String symbolsToString(ArrayList<Symbol> symbols) {
			String line = "";
			int i=0;
			while(i!=symbols.size()) {
				line=line+symbols.get(i);
				i++;
			}
			return line;
		}
	public void parseText() {
		String line = symbolsToString(this.symbols);
		text = new Text(line);
	}
	
	public char findMaxSymbol() {
		int counter = 1;
		char temp=' ';
		char mostFrequent=' ';
		int i=0;
		int tempCounter = 0;
		for (Sentence othersentences : text.getSentences()) {
			for (Word otherword : othersentences.getWords()) {
				temp=otherword.getValue().get(i).getSymbol();
				for (Symbol symb : otherword.getValue()) {
					if (Arrays.asList('a','e','i','o','u').contains(symb.getSymbol())) {
						if (temp==symb.getSymbol())
							tempCounter++;
						if (tempCounter>counter) {
							mostFrequent=temp;
							counter=tempCounter;
						}
					}
					i++;
				}
				i=0;
			}
		}
		System.out.println("The most often found vowel: "+mostFrequent);
		return mostFrequent;
	}
	
	public void findWordOfMaxSymbol() {
		char mostFrequent = findMaxSymbol();
		String words = ""; 
		for (Sentence othersentences : text.getSentences()) {
			for (Word otherword : othersentences.getWords()) {
				for (Symbol othersymbol : otherword.getValue()) {
					if (Arrays.asList(mostFrequent).contains(othersymbol.getSymbol())) {
						words+=otherword.toString()+", ";
					}
				}
			}
		}
		if (words !="")
			words=words.substring(0, words.length()-2)+'.';
		else
			words="Not found...";
		System.out.println("Found in words:");
		System.out.println(words);
	}
	
	public void outTable() {
		table = new String[2*text.getCountSentences()+3];
		table[0]=setStartLine();
		table[1]=setText( "�","Sentences","Word's Count"); 
		table[2]=setLine();
		findText();
		for (int i=0; i<table.length; i++) {
			System.out.println(table[i]);
		}
	}
	
	public String setStartLine() {
		String startline = "";
		for (int i=0; i<size; i++) {
			if (i==0 || i==size-1)
				startline+='|';
			else
				startline+='-';
		}
		return startline;
	}
	
	public String setLine() {
		String line = "";
		for (int i=0; i<size; i++) {
			if (i==0 || i==size-1)
				line+='|';
			else
				if (i==sizeRow1+1 || i==sizeRow1+length-2)
					line+='+';
				else
					line+='-';
		}
		return line;
	}
	
	public String setText(String num, String words, String count) {
		String rightIntervalRow2;
		int leftIntervalRow2 = (length- words.length()-4);
		int modifiedInterval;
		modifiedInterval=leftIntervalRow2;
		leftIntervalRow2=leftIntervalRow2/2;
		modifiedInterval=modifiedInterval-leftIntervalRow2;
		if (leftIntervalRow2==0) {
			rightIntervalRow2="";
			leftIntervalRow2++;
		}
		else
			rightIntervalRow2=Integer.toString(modifiedInterval);
		
		int rightIntervalRow3=(sizeRow2-count.length());
		int leftIntervalRow3=rightIntervalRow3;
		rightIntervalRow3 = rightIntervalRow3/2;
		leftIntervalRow3=leftIntervalRow3-rightIntervalRow3;
		String d="|%3s%2s|%"+leftIntervalRow2+"s%s"+"%"+rightIntervalRow2+"s|%"+rightIntervalRow3+"s%s%"+leftIntervalRow3+"s|";
		d=String.format(d, num,"", "",words,"","",count,"");  
		return d;
	}
	
	
	
	
	public void findText() {
		String words = "";
		int i=1;
		int t=1;
		int k=0;
		for (Sentence othersentence : text.getSentences()) {
			for (Element word : othersentence.getSentence()) {
				if (word.getClass().getSimpleName().equals("Word")) {
					if ((words.length()+word.toString().length()) < length-5) {
						words+=word.toString()+" ";
						k++;
					}
					else 
						break;
				}
			}
			
			table[i+2]=setText(Integer.toString(t), words, Integer.toString(k));
			if (i != (text.getCountSentences()*2-1))
				table[i+3]=setLine();
			else
				table[i+3]=setStartLine();
			words="";
			k=0;
			i=i+2;
			t++;
		}
	}
	
	public int getLength() {
		return length;
	}
	
	
	
	
}
